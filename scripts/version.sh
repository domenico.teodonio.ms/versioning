#!/usr/bin/bash

# This script exports the following variables depending on the status of the repository
#   - VERSION_FULL
#   - VERSION
#   - VERSION_MAJOR
#   - VERSION_MINOR
#   - VERSION_PATCH
#   - VERSION_METADATA
#   - VERSION_PRERELEASE
#   - VERSION_COMMITS_AHEAD
#   - VERSION_DIRTY


VERSION_COMMIT_HASH=`git rev-parse HEAD`
if [[ "`git describe 2>&1`" =~ .*fatal:.* ]]
then
    VERSION_FULL='v0.0.0'
    VERSION='v0.0.0'
    VERSION_MAJOR='0'
    VERSION_MINOR='0'
    VERSION_PATCH='0'
    VERSION_METADATA='0'
    VERSION_PRERELEASE='0'
    VERSION_COMMITS_AHEAD='0'
    VERSION_DIRTY='0'
else
    VERSION=`git describe --abbrev=0 | sed -n 's v\{0,1\}\(.*\) v\1 p'`
    VERSION_FULL=`git describe --long | sed -n 's v\{0,1\}\(.*\) v\1 p'`
    __VERSION_TAGS=`git describe --abbrev=0`
    __PARSE_STRING="s_v\{0,1\}\([0-9]*\).*_\1_p"
    VERSION_MAJOR=`sed -n $__PARSE_STRING <<< "$__VERSION_TAGS"`
    __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.\([0-9]*\).*_\1_p"
    VERSION_MINOR=`sed -n $__PARSE_STRING <<< "$__VERSION_TAGS"`
    __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.\([0-9]*\).*_\1_p"
    VERSION_PATCH=`sed -n $__PARSE_STRING <<< "$__VERSION_TAGS"`
    __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH-\([-\.a-zA-Z0-9]*\).*_\1_p"
    VERSION_PRERELEASE=`sed -n $__PARSE_STRING <<< "$__VERSION_TAGS"`
    VERSION_PRERELEASE_ESCAPED=${VERSION_PRERELEASE//\./\\.}
    if [[ -z $VERSION_PRERELEASE ]]
    then
        __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH+\([-\.a-zA-Z0-9]*\).*_\1_p"
    else
        __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH-${VERSION_PRERELEASE//\./\\.}+\([-\.a-zA-Z0-9]*\).*_\1_p"
    fi
    VERSION_METADATA=`sed -n $__PARSE_STRING <<< "$__VERSION_TAGS"`
    VERSION_METADATA_ESCAPED=${VERSION_METADATA//\./\\.}
    __VERSION_TAGS=`git describe --long`

    if [[ -z $VERSION_METADATA ]]
    then
        if [[ -z $VERSION_PRERELEASE ]]
        then
            __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH-\([0-9]*\).*_\1_p"
        else
            __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH-${VERSION_PRERELEASE//\./\\.}-\([0-9]*\).*_\1_p"
        fi
    else
        if [[ -z $VERSION_PRERELEASE ]]
        then
            __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH+${VERSION_METADATA//\./\\.}-\([0-9]*\).*_\1_p"
        else
            __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH-${VERSION_PRERELEASE//\./\\.}+${VERSION_METADATA//\./\\.}-\([0-9]*\).*_\1_p"
        fi
    fi
    VERSION_COMMITS_AHEAD=`sed -n $__PARSE_STRING <<< "$__VERSION_TAGS"`
    [[ -z VERSION_COMMITS_AHEAD ]] && VERSION_COMMITS_AHEAD='0'
    if [[ -z $VERSION_METADATA ]]
    then
        if [[ -z $VERSION_PRERELEASE ]]
        then
            __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH-$VERSION_COMMITS_AHEAD-g\([a-zA-Z0-9]*\).*_\1_p"
        else
            __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH-${VERSION_PRERELEASE//\./\\.}-$VERSION_COMMITS_AHEAD-g\([a-zA-Z0-9]*\).*_\1_p"
        fi
    else
        if [[ -z $VERSION_PRERELEASE ]]
        then
            __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH+${VERSION_METADATA//\./\\.}-$VERSION_COMMITS_AHEAD-g\([a-zA-Z0-9]*\).*_\1_p"
        else
            __PARSE_STRING="s_v\{0,1\}$VERSION_MAJOR\.$VERSION_MINOR\.$VERSION_PATCH-${VERSION_PRERELEASE//\./\\.}+${VERSION_METADATA//\./\\.}-$VERSION_COMMITS_AHEAD-g\([a-zA-Z0-9]*\).*_\1_p"
        fi
    fi
    VERSION_DIRTY=`git describe --dirty | sed -n 's *dirty* 1 p'`
    [[ -z $VERSION_DIRTY ]] && VERSION_DIRTY='0'

    unset __PARSE_STRING
    unset __VERSION_TAGS
    unset VERSION_METADATA_ESCAPED
    unset VERSION_PRERELEASE_ESCAPED
fi

export VERSION_MAJOR
export VERSION_MINOR
export VERSION_PATCH
export VERSION_PRERELEASE
export VERSION_METADATA
export VERSION_COMMITS_AHEAD
export VERSION_COMMIT_HASH
export VERSION_DIRTY
