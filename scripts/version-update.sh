#!/usr/bin/bash

source scripts/version.sh

for file in $(find templates/version -type f)
do
    [[ -d `dirname ${file#"templates/version/"}` ]] || mkdir -p `dirname ${file#"templates/version/"}` 
    cat $file | envsubst > ${file#"templates/version/"}
done
echo -n $VERSION_FULL > VERSION
