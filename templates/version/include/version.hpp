namespace version {
    inline constexpr auto major { ${VERSION_MAJOR} };
    inline constexpr auto minor { ${VERSION_MINOR} };
    inline constexpr auto patch { ${VERSION_PATCH} }; 
}
