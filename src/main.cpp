#include <cstdio>

#include <version.hpp>

int main() {
    printf("version: %d.%d.%d", version::major, version::minor, version::patch);
}
